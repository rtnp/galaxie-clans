#!/usr/bin/env python3
import os
import sys
import re

STATE_TEXT = 0
STATE_YAML = 1


def setup_patterns():
    class Struct:
        """Stores data attributes for dotted-attribute access."""

        def __init__(self, **keywordargs):
            self.__dict__.update(keywordargs)

    enum = Struct()
    enum.formatinfo = {
        "parens": Struct(prefix="(", suffix=")", start=1, end=-1),
        "rparen": Struct(prefix="", suffix=")", start=0, end=-1),
        "period": Struct(prefix="", suffix=".", start=0, end=-1),
    }
    enum.formats = enum.formatinfo.keys()
    enum.sequences = [
        "arabic",
        "loweralpha",
        "upperalpha",
        "lowerroman",
        "upperroman",
    ]  # ORDERED!
    enum.sequencepats = {
        "arabic": "[0-9]+",
        "loweralpha": "[a-z]",
        "upperalpha": "[A-Z]",
        "lowerroman": "[ivxlcdm]+",
        "upperroman": "[IVXLCDM]+",
    }

    pats = {}
    pats["nonalphanum7bit"] = "[!-/:-@[-`{-~]"
    pats["alpha"] = "[a-zA-Z]"
    pats["alphanum"] = "[a-zA-Z0-9]"
    pats["alphanumplus"] = "[a-zA-Z0-9_-]"
    pats["enum"] = (
        "(%(arabic)s|%(loweralpha)s|%(upperalpha)s|%(lowerroman)s"
        "|%(upperroman)s|#)" % enum.sequencepats
    )

    for format in enum.formats:
        pats[format] = "(?P<%s>%s%s%s)" % (
            format,
            re.escape(enum.formatinfo[format].prefix),
            pats["enum"],
            re.escape(enum.formatinfo[format].suffix),
        )

    patterns = {
        "bullet": u"[-+*\u2022\u2023\u2043]( +|$)",
        "enumerator": r"(%(parens)s|%(rparen)s|%(period)s)( +|$)" % pats,
    }
    for name, pat in patterns.items():
        patterns[name] = re.compile(pat)
    return patterns


PATTERNS = setup_patterns()


def get_indent(line):
    stripped_line = line.lstrip()
    indent = len(line) - len(stripped_line)
    if PATTERNS["bullet"].match(stripped_line) or PATTERNS["enumerator"].match(
        stripped_line
    ):

        indent += len(stripped_line.split(None, 1)[0]) + 1
    return indent


def get_stripped_line(line, strip_regex):
    if strip_regex:
        line = re.sub(strip_regex, "", line)
    return line


def convert(lines, strip_regex=None, yaml_strip_regex=None):
    state = STATE_TEXT
    last_text_line = ""
    last_indent = ""
    for line in lines:
        line = line.rstrip()
        if not line:
            # do not change state if the line is empty
            yield ""
        elif line.startswith("# ") or line == "#":
            if state != STATE_TEXT:
                yield "```"
            line = get_stripped_line(line, strip_regex)
            line = last_text_line = line[2:]
            yield line
            last_indent = get_indent(line) * " "
            state = STATE_TEXT
        elif line == "---":
            pass
        else:
            if line.startswith("---"):
                line = line[3:]
            if state != STATE_YAML:
                if not last_text_line.endswith("```"):
                    yield last_indent + "```"
                yield ""
            line = get_stripped_line(line, yaml_strip_regex)
            yield last_indent + line
            state = STATE_YAML
    if state != STATE_TEXT:
        yield "```"


sys.path.insert(0, os.getenv("DIRENV_PYTHON_LIBS_DIR"))

roles_src_path = os.getenv("ANSIBLE_ROLES_PATH")
if not roles_src_path:
    roles_src_path = "../../roles"
roles_doc_path = "."

for element in os.listdir(roles_src_path):

    if not os.path.isdir(roles_src_path + "/" + element + "/defaults"):
        continue

    for path, subdirs, files in os.walk(
        os.path.join(roles_src_path, element, "defaults")
    ):

        for filename in files:

            if filename.startswith("."):
                continue

            defaults_file = os.path.join(path, filename)
            defaults_dir = roles_doc_path + "_" + element

            # print("{0}".format(defaults_file))

            strip_regex = r"\s*(:?\[{3}|\]{3})\d?$"
            yaml_strip_regex = r"^\s{66,67}#\s\]{3}\d?$"
            with open(defaults_file) as infh:
                # with open(outfilename, "w") as outfh:
                for line_item in convert(
                    infh.readlines(), strip_regex, yaml_strip_regex
                ):
                    # print(l.rstrip(), file=outfh)
                    print(line_item.rstrip())
