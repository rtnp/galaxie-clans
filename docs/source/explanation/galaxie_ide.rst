#################
Galaxie-clans IDE
#################

Our galaxie-clans ide is based on nvim and aims at being the lightest implementation for the 
basic needs of working on the project. 

By chance, this setup also fits the needs for IT workers:

- python developers
- DevOps
- SRE
- System administrators

Description
***********

- LSP based supported
- Omni Completion
- Dark Theme
- AutoSave
- Integrate LazyGit
- Integrate LazyDocker
- FileExplorer left bar

Supported languages and files
=============================

- Python
- GoLang
- Ansible
- Yaml
- Docker
- Markdown
- RST

Strongly inspired by:

- SpaceVim: https://spacevim.org/quick-start-guide/
- LunarVim: https://github.com/LunarVim/LunarVim

References
**********

Notes for archives.

LSP manual installation
=======================

- tsserver: ``npm i -g typescript typescript-language-server prettier eslint_d``
- texlab: ``paru texlive-core texlab``
- rust: ``paru rust-analysis``
- python: ``pip install python-language-server``
- vimls: ``npm i -g vim-language-server``
- jsonls: ``npm i -g vscode-json-languageserver``
- html: ``npm i -g vscode-html-languageserver-bin``
- yaml: ``npm i -g yaml-language-server``
- bashls: ``npm i -g bash-language-server``
- cssls: ``npm i -g vscode-css-languageserver-bin``
- summeko_lua: ``paru lua-language-server``

LSP Shortcut
============

- `gD`: Goto Declaration
- `gd`: Goto Definition
- `K`: Hover
- `gi`: Implementation
- `<C-k>`: signature_help
- `<leader>wa`: add_workspace_folder
- `<leader>wr`: remove_workspace_folder
- `<leader>wl`: '<cmd>lua print(vim.inspect(vim.lsp.buf.list_workspace_folders()))<CR>`: opts)
- `<leader>D`: type_definition
- `<leader>rn`: rename  st  sf sf
- `gr`: references
- `<leader>ca`: code_action
- `<leader>e`: diagnostic.open_float
- `[d`: diagnostic.goto_prev
- `]d`: diagnostic.goto_next
- `<leader>q`: diagnostic.setloclist
- `<leader>so` require('telescope.builtin').lsp_document_symbols()<CR>]], opts)
- .cmd [[ command! Format execute 'lua vim.lsp.buf.formatting()' ]]

Telescop
========

- ``<leader><space>``: Buffers
- ``<leader>sf``: Find files
- ``<leader>sb``: Fuzzy find (current buffer)
- ``<leader>sh``: Help tags
- ``<leader>st``: Tags (Ultra slow)
- ``<leader>sd``: Grep string
- ``<leader>sp``: Live grep
- ``<leader>so``: Tags (current buffer)
- ``<leader>?``: oldfiles

