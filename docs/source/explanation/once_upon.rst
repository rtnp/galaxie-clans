*******************
Once Upon a time...
*******************

----

...in a not so far away cellar, after a session of music playing with our garage band: R.T.N.P. 
Mo (pure Dev) took his laptop on his knees (too many beers on the desk) to make a demo of a 
draft portage of the home automation scripts of Tuxa (pure Ops), assembled with Ansible and Vagrant.

That was the spark that started the whole Galaxie arc into the R.T.N.P initiative. "Galaxie" is named 
after the local network name of Tuxa at this time.

Tuxa took this codebase and finalized the refactoring until he could not bear anymore with code maintenance.
He called for help on Mo. They stabilized the actual form of the codebase and its principles and guidelines
in the first semester of 2020 (the first COVID-19 season in France).

----

...this project was hosted on a ancient platform called GitHub. 
Then came the Buyer. The Buyer bought GitHub, willing to rule over its community. 
We were not to sell, so here is the new home of https://github.com/Tuuux/galaxie 
and the project has much mutated since then...

We built an Ansible toolkit for managing a set of Debian servers, a set of 
powerful roles made hand-in-hand by a senior system admin and a senior developer.

Originaly written for managing a Home Network with external services. The project 
has shifted to managing digital survival of clanic entities.

Galaxie-Clans is a true mix of the two world, by luck author Tuuux and Mo have made the 
necessary skills and experience to accept no compromise.

.. admonition:: PRO TIPS
    :class: important

    Tech purity is cancer for the mind.

It would be an honor if you could use our work in any way. Be it as-is or as code samples.

Having fun since 2015-03-21, hack in peace.
