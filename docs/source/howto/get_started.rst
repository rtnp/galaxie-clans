.. _how_to_get_started:

*****************************************
Get started
*****************************************

Prerequisites
====================

Binary dependencies
--------------------

* ``git``
* ``make``
* `direnv <https://direnv.net/docs/installation.html>`_ 

Configuration
--------------------

* direnv `hooked to your shell <https://direnv.net/docs/hook.html>`_

Setup workspace
====================

* Run:

.. code:: bash

    git clone https://gitlab.com/rtnp/galaxie-clans.git
    cd galaxie-clans/
    make prepare-debian
    direnv allow
    make env

.. admonition:: CONGRATULATIONS
    :class: important

    You are ready to work with the project.
