********************************
HOWTO Contribute documentation?
********************************

Tutorials: Learning oriented
============================
Reference:
  * https://github.com/umlaeute/v4l2loopback
  * https://forums.opensuse.org/showthread.php/545656-v4l2loopback-resolution-How-to-store-configuration-across-reboots
  * https://awesomeopensource.com/project/CatxFish/obs-v4l2sink?categoryPage=10

Create a dedicated file
-----------------------
In interactive as Super User
.. code:: bash

    sudo modprobe videodev
    sudo modprobe v4l2loopback video_nr=42 devices=1 card_label="GLXCam"
    sudo modprobe snd-aloop

In interactive as Normal User
.. code:: bash
    v4l2loopback-ctl set-caps "video/x-raw, format=I420, width=1920, height=1080" /dev/video6
    v4l2loopback-ctl set-caps "video/x-raw, format=UYVY, width=1920, height=1080" /dev/video6
    v4l2loopback-ctl set-caps "video/x-raw, format=UYVY, width=1920, height=1080, framerate=(fraction)25/1" /dev/video42

/etc/modules_load.d/galaxie_stream.conf
.. code::
  videodev
  v4l2loopback
  snd-aloop


CHANGING THE RUNTIME BEHAVIOUR
------------------------------
FORCING FPS
.. code:: bash

    v4l2loopback-ctl set-fps /dev/video42 25

or
.. code:: bash

    echo '@100' | sudo tee /sys/devices/virtual/video4linux/video42/format

.. code:: bash

    v4l2-ctl -d /dev/video42 -c timeout=3000

or
.. code:: bash

    v4l2loopback-ctl set-timeout-image -t 3000 /dev/video0 service-unavailable.png


LOAD THE MODULE AT BOOT
-----------------------
.. code:: bash
cat <<EOF > /etc/modules-load.d/v4l2loopback.conf
videodev
v4l2loopback
snd-aloop
EOF

.. code:: bash
cat <<EOF > /etc/modprobe.d/v4l2loopback.conf
options v4l2loopback devices=1
options v4l2loopback video_nr=42
options v4l2loopback card_label="V4l2 Camera"
options v4l2loopback exclusive_caps=1
EOF

.. code:: bash
    update-initramfs
