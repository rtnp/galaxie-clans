*****************************************
Use galaxie-clans as a dependency
*****************************************

Install galaxie-clans collection
================================

.. important::

    Mind about setting your Ansible `collections paths <https://docs.ansible.com/ansible/latest/reference_appendices/config.html#collections-paths>`_.
    The first path in this list will be used as install path.


One-time install
----------------

* Run:

.. code:: bash

    ansible-galaxy collection install rtnp.galaxie_clans

Add to your project dependencies
--------------------------------

* Create a ``requirements.yml`` file in your project with this content:

.. code:: yaml

    collections:
      - name: rtnp.galaxie_clans

* From this on, you will be able to install the collection by running:

.. code:: bash

    ansible-galaxy collection install -fr requirements.yml

Use the collection's roles
================================

Once the collection is installed.lea You can use the collections's roles in your playbooks.

For example:

.. code:: yaml

    - hosts: targets
      gather_facts: yes

      roles:
        - rtnp.galaxie_clans.system_base

To learn more about using collections, see 
`Ansible documentation <https://docs.ansible.com/ansible/latest/user_guide/collections_using.html>`_.
