# Galaxie-Clans

This project is an opinionated Ansible collection living on [Gitlab](https://gitlab.com/rtnp/galaxie-clans>).

Every gamer needs a [clan](https://en.wikipedia.org/wiki/Clan).

Find here everything a gamer clan needs to live in the [Information Age](https://en.wikipedia.org/wiki/Information_Age).

## Who is it for?

* Litteraly every group of friends willing to communicate over Internet.

## Why even bother to do this?

* Because we can.
* Because Open-Source is cool.
* For the fun.

## Lost?

```{toctree}
:maxdepth: 1

tutorials
howto
explanation
reference
```

----
```{admonition} Digging deeper
:class: note

The documentation structure follows the [Diataxis framework](https://diataxis.fr/) guidance.
```