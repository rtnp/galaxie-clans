# Ansible core roles

```{toctree}
:maxdepth: 1

role_host
role_system_base
role_system_users
role_dns
role_certs
role_mailserver
role_rproxy
role_monitor
```
