
```{include} ../../../roles/certs/README.md
```

## Role defaults

Enable/disable applying this role.

```

glxclans_certs_enable: false
```

Mode of certificate management. Must be one of: [custom, acme]

```

glxclans_certs_mode: ~

glxclans_certs_hostname: "{{ ansible_hostname }}"
glxclans_certs_domain_name: "{{ glxclans_system_base_domain }}"
```

* Local path where generated secrets are to be generated and read.

```

glxclans_certs_secrets_dir: >-
  {{
    glxclans_group_secrets_dir
    | default(inventory_dir + '/group_vars/glxclans/secrets')
  }}
glxclans_certs_host_altnames:
  - "DNS:*.{{ glxclans_certs_domain_name }}"
  - "DNS:{{ glxclans_certs_domain_name }}"
  - "DNS:{{ glxclans_certs_hostname }}"
  - "DNS:{{ __glxclans_certs_fqdn }}"
  - "DNS:localhost"
  - "IP:127.0.0.1"
```

* Local path where each node private key should be generated.

```

glxclans_certs_host_private_key: "{{ glxclans_certs_secrets_dir }}/{{ __glxclans_certs_fqdn }}.key"
```

* Local path where each node certificate should be generated.

```

glxclans_certs_host_cert: "{{ glxclans_certs_secrets_dir }}/{{ __glxclans_certs_fqdn }}.crt"
```

* Local path where each node fullchain certificate should be generated.

```

glxclans_certs_host_fullchain_cert: "{{ glxclans_certs_secrets_dir }}/{{ __glxclans_certs_fqdn }}.fullchain.crt"


```
## OwnCA root informations

If `glxclans_certs_ownca_root_cert` and `glxclans_certs_ownca_root_private_key` are existing files, the generation
will be skipped.

```

glxclans_certs_ownca_root_cert: >-
  {{ glxclans_certs_secrets_dir }}/{{ __glxclans_certs_fqdn }}.ownca.root.crt
glxclans_certs_ownca_root_private_key: >-
  {{ glxclans_certs_secrets_dir }}/{{ __glxclans_certs_fqdn }}.ownca.root.key
glxclans_certs_ownca_root_private_keypass: >-
  {{ glxclans_certs_secrets_dir }}/{{ __glxclans_certs_fqdn }}.ownca.root.keypass
glxclans_certs_ownca_root_private_keypass_content: >-
  {{ lookup('password', glxclans_certs_ownca_root_private_keypass + ' length=42') }}
```

## Intermediate CA certificate

Will be generated and signed by the root CA certificate.
Will be added to the target hosts trust store.

```

glxclans_certs_ownca_inter_cert: >-
  {{ glxclans_certs_secrets_dir }}/{{ __glxclans_certs_fqdn }}.ownca.inter.crt
glxclans_certs_ownca_inter_private_key: >-
  {{ glxclans_certs_secrets_dir }}/{{ __glxclans_certs_fqdn }}.ownca.inter.key
glxclans_certs_ownca_inter_private_keypass: >-
  {{ glxclans_certs_secrets_dir }}/{{ __glxclans_certs_fqdn }}.ownca.inter.keypass
glxclans_certs_ownca_inter_private_keypass_content: >-
  {{ lookup('password', glxclans_certs_ownca_inter_private_keypass + ' length=42') }}
```

## ACME certificates requests

* Must be a list of dict, each item following with these attributes:

```
- cn: "mx1.clans.galaxie.family"      # Mandatory. Main FQDN to certify.
  zone: "clans.galaxie.family"        # Mandatory. Host's managed zones that will handle the DNS challenge.
  alt:                                # Optional. If you want your cert to be multi-FQDN
    - "DNS:mail.clans.galaxie.family"
    - "DNS:mail.rtnp.org"
  alt_multi_domain:                    # Optional. Set only if the 'alt' attribute contains multiple domains
                                       # must dispatch the challenges amongst host's managed zones.
    - record: "_acme-challenge.mail.clans.galaxie.family"
      zone: "clans.galaxie.family"
    - record: "_acme-challenge.mail.rtnp.org"
      zone: "rtnp.org"
```

glxclans_certs_acme_requests: []
```

* The DNS authority server that will be nsupdated with records of the DNs challenge.

```

glxclans_certs_acme_nsupdate_host: "127.0.0.1"
glxclans_certs_acme_nsupdate_tsig_key_name: ~
glxclans_certs_acme_nsupdate_tsig_key_secret: ~
glxclans_certs_acme_nsupdate_tsig_key_algorithm: ~
