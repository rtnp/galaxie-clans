
```{include} ../../../roles/host/README.md
```

```

glxclans_host_workspace: "{{ glxclans_workspace | default(inventory_dir) }}"
glxclans_host_service_user_default_key_dir: "group_vars/glxclans/secrets"
```

### Host

* Short name that will be enforced as hostname (should be [RFC-1035](https://www.rfc-editor.org/rfc/rfc1035) compliant).

```

glxclans_host_name: "{{ inventory_hostname | replace('_', '-') }}"
```

* Set this to `true` if you want the role to generate:
    * an ssh configuration block into the `{{ glxclans_host_workspace }}/ssh.cfg` local file.
    * an insertion of the `{{ glxclans_host_name }}` into the `[glxclans]` inventory group.

```

glxclans_host_genconfig: false
```

### Service account

```

glxclans_host_service_user_name: "caretaker"
glxclans_host_service_user_shell: "/bin/bash"
glxclans_host_service_user_home: "/home/{{ glxclans_host_service_user_name }}"
glxclans_host_service_user_uid: "4200"
glxclans_host_service_user_gid: "4200"
glxclans_host_service_user_default_private_key_file: >-
  {{ glxclans_host_service_user_default_key_dir }}/{{ glxclans_host_name }}.key
glxclans_host_service_user_default_public_key_file: >-
  {{ glxclans_host_service_user_default_private_key_file }}.pub
glxclans_host_service_user_authorized_keys:
  - "{{ lookup('file', glxclans_host_workspace + '/' + glxclans_host_service_user_default_public_key_file) }}"
```

## SSH configuration

```

glxclans_host_ssh:
  allow_tcp_forwarding: "yes"
  authorized_keys_file: "%h/.ssh/authorized_keys"
  challenge_response_authentication: "yes"
  client_alive_count_max: "2"
  client_alive_interval: "300"
  compression: "delayed"
  gssapi_authentication: "no"
  hostbased_authentication: "no"
  ignore_rhosts: "yes"
  listen_address: "0.0.0.0"
  log_level: "INFO"
  login_grace_time: "60"
  password_authentication: "yes"
  permit_empty_passwords: "no"
  permit_root_login: "no"
  protocol: "2"
  pubkey_authentication: "yes"
  strict_modes: "yes"
  syslog_facility: "AUTH"
  tcp_keep_alive: "yes"


glxclans_system_base_hostname: "{{ inventory_hostname | replace('_', '-') }}"
glxclans_system_base_domain: "{{ ansible_hostname | default('clans.galaxie.family') }}"
glxclans_system_base_language: 'fr_FR'
glxclans_system_base_timezone: "Europe/Paris"
glxclans_system_base_ntp_server_upper_stratum:
  - "0.pool.ntp.org"
  - "1.pool.ntp.org"
  - "2.pool.ntp.org"
  - "3.pool.ntp.org"
glxclans_system_base_ntp_restrict:
  - "127.0.0.1"
  - "::1"
glxclans_system_base_pip_packages:
  - "ansible-core"
  - "bcrypt"
  - "bpytop"
  - "cryptography"
  - "dnspython"
  - "passlib"
  - "duplicity"
glxclans_system_base_sysctl_vm_swappiness: "10"
glxclans_system_base_sysctl_vm_vfs_cache_pressure: "50"
glxclans_system_base_sysctl_vm_overcommit_memory: "0"
glxclans_system_base_sysctl_vm_overcommit_ratio: "95"
glxclans_system_base_sysctl_net_core_wmem_max: "1048576"
glxclans_system_base_sysctl_net_core_rmem_max: "10485760"
glxclans_system_base_vimrc:
  syntax: "on"
  background: "dark"
  tabstop: "4"
  shiftwidth: "4"
  expandtab: true
  noautoindent: true
  mouse: ""
  ttymouse: ""
glxclans_system_base_banner_text: "Galaxie Clans"
glxclans_system_base_ssl_certs_dir: "/etc/ssl/private"

