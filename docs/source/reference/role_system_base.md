
```{include} ../../../roles/system_base/README.md
```

## Defaults

### Host

Defaults to a variation of ``inventory_hostname`` because
underscores are not valid characters in host names.

```

glxclans_system_base_hostname: "{{ inventory_hostname | replace('_', '-') }}"
glxclans_system_base_domain: "{{ ansible_hostname | default('clans.galaxie.family') }}"
```

### Locale

```

glxclans_system_base_language: 'fr_FR'
```

### Time configuration

```

glxclans_system_base_timezone: "Europe/Paris"
glxclans_system_base_ntp_server_upper_stratum:
  - "0.pool.ntp.org"
  - "1.pool.ntp.org"
  - "2.pool.ntp.org"
  - "3.pool.ntp.org"
glxclans_system_base_ntp_restrict:
  - "127.0.0.1"
  - "::1"
```

### System packages


### Python packages

```

glxclans_system_base_pip_packages:
  - "ansible-core"
  - "bcrypt"
  - "bpytop"
  - "cryptography"
  - "dnspython"
  - "passlib"
  - "duplicity"
```

### Sysctl configuration

```

glxclans_system_base_sysctl_vm_swappiness: "10"
glxclans_system_base_sysctl_vm_vfs_cache_pressure: "50"
glxclans_system_base_sysctl_vm_overcommit_memory: "0"
glxclans_system_base_sysctl_vm_overcommit_ratio: "95"
glxclans_system_base_sysctl_net_core_wmem_max: "1048576"
glxclans_system_base_sysctl_net_core_rmem_max: "10485760"
```

### Vim configuration

```

glxclans_system_base_vimrc:
  syntax: "on"
  background: "dark"
  tabstop: "4"
  shiftwidth: "4"
  expandtab: true
  noautoindent: true
  mouse: ""
  ttymouse: ""
```

### Banner and Message Of The Day

```

glxclans_system_base_banner_text: "Galaxie Clans"
```

### Private certificate store

The role will create a system user group 'ssl-cert' and only users belonging
to this group will have access to the content of the ``{{ system_base_ssl_certs_dir }}`` directory.

```

glxclans_system_base_ssl_certs_dir: "/etc/ssl/private"

