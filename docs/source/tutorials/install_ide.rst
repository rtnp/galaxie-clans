#########################
Install galaxie-clans IDE
#########################

This tutorial will lead you through installing the IDE we use to develop the project, based on `neovim <https://neovim.io/>`_. 

Ready?
******

* Your current user has sudo rights

Set.
****

Go!
***

.. code:: bash

    make ide

----

.. admonition:: CONGRATULATIONS
    :class: important

    Now you can start your ide with the ``nvim`` command.

For further explanations on the IDE itself and how to become productive with it, see :doc:`/explanation/galaxie_ide` 
