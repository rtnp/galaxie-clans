######
Turn Kodi into a retro gaming interface


deb https://www.deb-multimedia.org bullseye main non-free

apt-get update -oAcquire::AllowInsecureRepositories=true
apt-get install deb-multimedia-keyring


plugin iagl

https://github.com/zach-morris/plugin.program.iagl

get_url: https://github.com/zach-morris/repository.zachmorris/raw/master/repository.zachmorris/repository.zachmorris-1.0.0.zip

lancer retroarch en root : mises à jour, install des coeurs

premier lancement en user-space, vérifier que le coeur est dispo

installer le bios lynx dans ~/.config/retroarch/system

OpenEmu bios pack sur internet archive

get_url: https://archive.org/download/OpenEmuBIOSPack/OpenEmu%20BIOS%20Pack.zip
