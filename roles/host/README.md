# rtnp.galaxie_clans.host

```{admonition} Note
:class: important
* __This role is a prerequisites for any hosts.__
* It should be run by a playbook with `gather_facts: false`, as it does not rely on existing 
python installation.
* Facts gathering is made as a task during the role, once the mandatory system package are installed by the role.
```

## Responsibility

* Installs mandatory packages to avoid troubles managing the system.
* Installs a service account with correct sudo privileges and authorized keys.
* Configures SSH daemon for security best-practices

## Supports

* Debian 12
* Rocky Linux 9
* Oracle Linux 9
