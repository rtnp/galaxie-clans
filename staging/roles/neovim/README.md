# neovim

* Installs NeoVim
* Configures NeoVim Mollasse
* Installs LazyGit ( `Space` `t` `G` )
* Installs LazyDocker ( `Space` `t` `D` )
* Installs LazyNPM ( `Space` `t` `N` )
* Install programming fonts `JetBrainMono` and `Monokai`

