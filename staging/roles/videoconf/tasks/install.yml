---
- shell: >-
    cat /etc/timezone
  changed_when: no
  register: _jitsi_meet_system_tz

- name:
  apt:
    name:
      - docker-compose
      - coturn

- name: Pre-pull images
  docker_image:
    name: "{{ item.value }}"
    source: pull
  loop: "{{ jitsi_meet_docker_images | dict2items }}"

- name: Create jitsi-meet directories
  file:
    path: "{{ item }}"
    state: directory
  loop: "{{ jitsi_meet_expected_directories }}"

- name: Render interface configuration file
  template:
    src: "interface_config.js.j2"
    dest: "{{ jitsi_meet_nginx_web_dir }}/interface_config.js"
  notify:
    - Restart jitsi-meet service

- name: Render web configuration file
  template:
    src: "config.js.j2"
    dest: "{{ jitsi_meet_nginx_web_dir }}/config.js"
  notify:
    - Restart jitsi-meet service

- name: Adjust sysctl rmem_max and rmem_default
  sysctl:
    name: "{{ item }}"
    value: "{{ jitsi_meet_sysctl_rmem_max }}"
    sysctl_set: yes
    state: present
    reload: yes
  loop:
    - "net.core.rmem_default"
    - "net.core.rmem_max"
  notify:
    - Restart jitsi-meet service

- name: Render jvb udp configuration file
  template:
    src: "20-jvb-udp-buffers.conf.j2"
    dest: "{{ jitsi_meet_jvb_dir }}/20-jvb-udp-buffers.conf"
  notify:
    - Restart jitsi-meet service

- name: Render jvb sip-communicator configuration file
  template:
    src: "sip-communicator.properties.j2"
    dest: "{{ jitsi_meet_jvb_dir }}/sip-communicator.properties"
  notify:
    - Restart jitsi-meet service

- name: Render turnserver configuration file
  template:
    src: "turnserver.conf.j2"
    dest: "/etc/turnserver.conf"
  notify:
    - Restart coturn service

- name: Render docker-compose file
  template:
    src: "docker-compose.yml.j2"
    dest: "{{ jitsi_meet_docker_compose_file }}"
  notify:
    - Restart jitsi-meet service

- name: Render nginx configuration
  template:
    src: "nginx.conf.j2"
    dest: "/etc/nginx/sites-available/jitsi-meet.conf"

- name: Render docker-compose environment file
  template:
    src: "config.env.j2"
    dest: "{{ jitsi_meet_configuration_file }}"
  notify:
    - Restart jitsi-meet service

- name: Render jitsi-meet systemd service
  template:
    src: "systemd.service.j2"
    dest: "{{ jitsi_meet_systemd_service_file }}"
  register: _jitsi_meet_systemd_service_render
  notify:
    - Restart jitsi-meet service

- name: Start jitsi-meet service
  systemd:
    name: jitsi-meet
    state: started
    daemon_reload: "{{ _jitsi_meet_systemd_service_render is changed }}"

#- name: Render nginx configuration file
