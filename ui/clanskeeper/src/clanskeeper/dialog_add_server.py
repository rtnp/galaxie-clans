from PyQt5.QtWidgets import QWizard
from PyQt5.QtGui import QPixmap
from wizard_add_host_ui import Ui_Wizard

import qdarktheme
class DialogAddServer(QWizard, Ui_Wizard):

    def __init__(self):
        super(DialogAddServer, self).__init__()

        self.setupUi(self)
        self.setPixmap(QWizard.BackgroundPixmap, QPixmap(":/galaxie_logo_nord10.png"))


