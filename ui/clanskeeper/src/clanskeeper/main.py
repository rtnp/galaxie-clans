#!/usr/bin/env python3

import os
import sys

from sshconf import read_ssh_config

from PyQt5.QtCore import Qt
from PyQt5.QtGui import QKeySequence, QStandardItemModel, QStandardItem, QIcon
from PyQt5.QtWidgets import (
    QApplication,
    QMainWindow,
    QFileDialog,
    QTreeView,
    QMessageBox,
    QTreeWidgetItem,
    QMenu,
    QAction,

)
import qdarktheme
from clanskeeper.main_window_ui import Ui_MainWindow
from clanskeeper.dialog_add_server import DialogAddServer
from clanskeeper.dialog_network_utility import DialogNetworkUtility


class Window(QMainWindow, Ui_MainWindow):
    MainTreeView: QTreeView

    def __init__(self, parent=None):
        super().__init__(parent)
        self.fileName = None
        self.MainTreeViewModel = None
        self.MainTreeWidgetUsers = None
        self.MainTreeWidgetServers = None
        self.network_utility_dialog = None

        self.setupUi(self)
        self.setupCustomUI()
        self.connectSignalsSlots()
        self.setWindowModified(True)

    def setupCustomUI(self):
        self.setWindowTitle('New document - ClansKeeper[*]')
        self.MainTreeWidget.sortByColumn(1, Qt.DescendingOrder)
        self.MainTreeWidgetUsers = QTreeWidgetItem(self.MainTreeWidget)
        self.MainTreeWidgetUsers.setText(0, "Users")
        self.MainTreeWidgetUsers.setIcon(0, QIcon.fromTheme("user"))
        self.MainTreeWidgetUsers.setExpanded(True)

        self.MainTreeWidgetServers = QTreeWidgetItem(self.MainTreeWidget)
        self.MainTreeWidgetServers.setText(0, "Servers")
        self.MainTreeWidgetServers.setIcon(0, QIcon.fromTheme("network"))
        self.MainTreeWidgetServers.setExpanded(True)

        self.actionAdd_a_server = QAction()
        self.actionAdd_a_server.setText("Add a server")
        self.MainPlusMenu = QMenu()
        self.MainPlusMenu.addAction(self.actionAdd_a_server)

    def connectSignalsSlots(self):
        self.actionOpen.triggered.connect(self.opendir)
        self.actionSave.triggered.connect(self.save)
        self.actionSave_As.triggered.connect(self.saveAs)
        # self.MainTreeWidget.currentItemChanged.connect(self.setWindowModified)
        self.actionAddServer.triggered.connect(self.show_add_server)
        self.actionToolsNetworkUtility.triggered.connect(self.show_network_utility)



    def show_network_utility(self):
        self.network_utility_dialog = DialogNetworkUtility()
        self.network_utility_dialog.show()

    def show_add_server(self):
        self.wizard_add_server = DialogAddServer()
        self.wizard_add_server.show()
        # self.wizard_add_server.setupUi(self)

    def opendir(self):
        my_dir = QFileDialog.getExistingDirectory(
            self,
            "Open a folder",
            os.path.curdir,
            QFileDialog.ShowDirsOnly
        )
        if "ssh.cfg" in os.listdir(my_dir):
            getdata = read_ssh_config(os.path.join(my_dir, 'ssh.cfg'))
            pos = 0
            for host in getdata.hosts():
                barA = QTreeWidgetItem(self.MainTreeWidgetServers)
                barA.setText(0, f"{host}")
                barA.setIcon(0, QIcon.fromTheme("server"))
        else:
            dlg = QMessageBox(self)
            dlg.setWindowTitle("No SSH File found")
            dlg.setText(f"{os.path.join(my_dir, 'ssh.cfg')} do not exist")
            dlg.exec()

    def save(self):
        if not self.isWindowModified():
            return
        if not self.fileName:
            self.saveAs()
        else:
            with open(self.fileName, 'w') as f:
                f.write(self.editor.toPlainText())

    def saveAs(self):
        if not self.isWindowModified():
            return
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        fileName, _ = QFileDialog.getSaveFileName(
            self,
            "Save File", "", "All Files(*);;Text Files(*.txt)", options=options
        )
        if fileName:
            with open(fileName, 'w') as f:
                f.write("Hello Galaxie Clans")
            self.fileName = fileName
            self.setWindowTitle(str(os.path.basename(fileName)) + " - ClansKeeper[*]")

    def closeEvent(self, event):
        super(Window, self).closeEvent(event)


def main():
    app = QApplication(sys.argv)
    # Apply dark theme.
    qdarktheme.setup_theme("auto")
    win = Window()

    # win.setStyleSheet(qdarktheme.load_stylesheet(theme="dark"))
    win.show()
    sys.exit(app.exec())


if __name__ == "__main__":
    main()
